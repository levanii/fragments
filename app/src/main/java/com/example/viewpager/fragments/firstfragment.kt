package com.example.viewpager.fragments

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpager.R

class firstfragment:Fragment(R.layout.fragment_first) {
    private lateinit var editText: EditText
    private lateinit var textView: TextView
    private lateinit var button: Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editText = view.findViewById(R.id.editText)
        textView = view.findViewById(R.id.note)
        button = view.findViewById(R.id.button)
        val sharedPreferences = requireActivity().getSharedPreferences("note", MODE_PRIVATE)
        textView.text = sharedPreferences.getString("notetext","")
        button.setOnClickListener{
            val note = textView.text.toString()
            val text = editText.text.toString()
            val result = note + "\n" + text
            textView.text = result
            sharedPreferences.edit()
                .putString("notetext","result").apply()
        }
    }
}