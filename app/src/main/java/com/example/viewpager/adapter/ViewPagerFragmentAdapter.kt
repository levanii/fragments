package com.example.viewpager.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpager.fragments.firstfragment
import com.example.viewpager.fragments.secondfragment
import com.example.viewpager.fragments.thirdfragment

class ViewPagerFragmentAdapter(activity:FragmentActivity):FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 3


    override fun createFragment(position: Int): Fragment {
       return when(position){
           0 -> firstfragment()
           1 -> secondfragment()
           2 -> thirdfragment()
           else -> firstfragment()
       }

    }
}